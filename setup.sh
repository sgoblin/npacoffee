#!/usr/bin/env bash

rm -r build
mkdir build
virtualenv -p python3 pEnv
npm install -g csso-cli html-minifier
npm link html-minifier
