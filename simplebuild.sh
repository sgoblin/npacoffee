#!/usr/bin/env bash

source pEnv/bin/activate
pip3 install Jinja2
python3 processing/templater.py
#uncss -s material.brown-green.min.1.2.1.css index.html > result.css
cat material.brown-green.min.1.2.1.css > result.css
#cat material-icons-3.0.1.css >> result.css
cat style.css >> result.css
csso result.css result.css --stat
python3 processing/insertstyle.py
node processing/minify.js
cp -R font index.html favicon.ico thanks.html build
rm result.css index.html
deactivate
