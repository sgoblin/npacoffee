from jinja2 import Template

with open('templateindex.html', 'r') as theFile:
    theTemplate = theFile.read()

with open('coffees.txt', 'r') as theFile:
    rawCoffee = theFile.read().split('\n')

with open('travelcolors.txt', 'r') as theFile:
    travelcolors = theFile.read().strip().split('\n')

with open('material-1.2.1.min.js', 'r') as theFile:
    mdljs = theFile.read()

coffees = []

for i in rawCoffee:
    i = i.split(':')
    coffees.append({'name': i[0], 'simplename': i[1]})

print(coffees)

template = Template(theTemplate)

with open('index.html', 'w') as theFile:
    theFile.write(template.render(coffee=coffees, style="{{style}}", mdljs=mdljs, travelcolors=travelcolors))
