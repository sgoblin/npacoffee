from jinja2 import Template

with open('index.html', 'r') as theFile:
    theTemplate = theFile.read()

with open('result.css', 'r') as theFile:
    style = theFile.read()

template = Template(theTemplate)

with open('index.html', 'w') as theFile:
    theFile.write(template.render(style=style))
